val kotlinNativeVer = "0.7.1"

rootProject.name = "openfaas-weather-sample"

pluginManagement {
    repositories {
        gradlePluginPortal()
        jcenter()
        maven("https://dl.bintray.com/jetbrains/kotlin-native-dependencies")
    }

    // This way we can map plugin to standard maven artifact
    // for maven repositories without plugin descriptor
    resolutionStrategy {
        eachPlugin {
        	/*
            if (requested.id.id.startsWith("kotlin-platform")) {
                useModule("org.jetbrains.kotlin:kotlin-gradle-plugin:1.2.50")
            }
            */
            if (requested.id.id == "konan") {
                useModule("org.jetbrains.kotlin:kotlin-native-gradle-plugin:$kotlinNativeVer")
            }
        }
    }
}
