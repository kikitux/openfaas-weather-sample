package org.example.weather_func.curl

import org.example.weather_func.Event
// size_t is a alias for Long.
import platform.posix.size_t

import curl.curl_easy_setopt as setCurlOption
import curl.CURLOPT_URL
import curl.CURLOPT_HEADERFUNCTION
import curl.CURLOPT_HEADERDATA
import curl.CURLOPT_WRITEFUNCTION
import curl.CURLOPT_WRITEDATA
import curl.curl_easy_cleanup as cleanupCurl
import curl.curl_easy_init as initCurl
import curl.CURLE_OK as CURL_ERROR_OK
import curl.curl_easy_strerror as curlErrorMessage
import curl.curl_easy_perform as curlFileSync

import kotlinx.cinterop.staticCFunction
import kotlinx.cinterop.COpaquePointer
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.StableRef
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.ByteVar
import kotlinx.cinterop.readBytes

internal class CUrl(val url: String) {
	private val stableRef = StableRef.create(this)
	private val curlObj = initCurl()
	val header = Event<String>()
    val body = Event<String>()

    init {
        val header = staticCFunction(::headerCallback)
        val writeData = staticCFunction(::writeCallback)

        setCurlOption(curlObj, CURLOPT_URL, url)
        setCurlOption(curlObj, CURLOPT_HEADERFUNCTION, header)
        setCurlOption(curlObj, CURLOPT_HEADERDATA, stableRef.asCPointer())
        setCurlOption(curlObj, CURLOPT_WRITEFUNCTION, writeData)
        setCurlOption(curlObj, CURLOPT_WRITEDATA, stableRef.asCPointer())
    }

    fun fetch() {
        // Have Curl do a HTTP/HTTPS request and store the response (status).
        val response = curlFileSync(curlObj)
        // length in bytes.
        val length = 8192 * 50

        // Print the error message if the Curl status code isn't OK (CURL_ERROR_OK).
        if (response != CURL_ERROR_OK) println("curlFileSync() failed: ${curlErrorMessage(response)?.toKString(length)}")
    }

	fun close() {
		cleanupCurl(curlObj)
		stableRef.dispose()
	}
}

fun headerCallback(buffer: CPointer<ByteVar>?, size: size_t, totalItems: size_t, userData: COpaquePointer?): size_t {
    var responseSize = 0L

    if (buffer != null && userData != null) {
        val header = buffer.toKString((size * totalItems).toInt()).trim()
        val tmpCurl = userData.asStableRef<CUrl>().get()

        tmpCurl.header(header)
        responseSize = size * totalItems
    }
    return responseSize
}

fun writeCallback(buffer: CPointer<ByteVar>?, size: size_t, totalItems: size_t, userData: COpaquePointer?): size_t {
    var responseSize = 0L

    if (buffer != null && userData != null) {
        val data = buffer.toKString((size * totalItems).toInt()).trim()
        val tmpCurl = userData.asStableRef<CUrl>().get()

        tmpCurl.body(data)
        responseSize = size * totalItems
    }
    return responseSize
}

private fun CPointer<ByteVar>.toKString(length: Int) = readBytes(length).stringFromUtf8()
